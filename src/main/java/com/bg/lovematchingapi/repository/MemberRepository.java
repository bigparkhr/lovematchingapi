package com.bg.lovematchingapi.repository;

import com.bg.lovematchingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
