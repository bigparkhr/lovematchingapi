package com.bg.lovematchingapi.repository;

import com.bg.lovematchingapi.entity.LoveLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoveLineRepository extends JpaRepository<LoveLine, Long> {
}
