package com.bg.lovematchingapi.service;

import com.bg.lovematchingapi.entity.LoveLine;
import com.bg.lovematchingapi.entity.Member;
import com.bg.lovematchingapi.model.LoveLine.LoveLineCreateRequest;
import com.bg.lovematchingapi.model.LoveLine.LoveLineItem;
import com.bg.lovematchingapi.model.LoveLine.LoveLinePhoneNumberChangeRequest;
import com.bg.lovematchingapi.repository.LoveLineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoveLineService {
    private final LoveLineRepository loveLineRepository;

    public void setLoveLine(Member member, LoveLineCreateRequest request) {
        LoveLine addData = new LoveLine();
        addData.setMember(member);
        addData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(addData);
    }

    public List<LoveLineItem> getLoveLines() {
        List<LoveLine> originList = loveLineRepository.findAll();

        List<LoveLineItem> result = new LinkedList<>();

        for (LoveLine loveLine: originList) {
            LoveLineItem addItem = new LoveLineItem();
            addItem.setMemberId(loveLine.getMember().getId());
            addItem.setMemberName(loveLine.getMember().getName());
            addItem.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
            addItem.setLovePhoneNumber(loveLine.getLovePhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public void petPhoneNumber(long id, LoveLinePhoneNumberChangeRequest request) {
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();
        originData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(originData);

    }
}
