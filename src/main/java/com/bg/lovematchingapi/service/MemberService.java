package com.bg.lovematchingapi.service;

import com.bg.lovematchingapi.entity.Member;
import com.bg.lovematchingapi.model.member.MemberCreateRequest;
import com.bg.lovematchingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setIsMan(request.getIsMan());

        memberRepository.save(addData);
    }
}
