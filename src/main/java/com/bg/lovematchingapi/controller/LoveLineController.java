package com.bg.lovematchingapi.controller;

import com.bg.lovematchingapi.entity.Member;
import com.bg.lovematchingapi.model.LoveLine.LoveLineCreateRequest;
import com.bg.lovematchingapi.model.LoveLine.LoveLineItem;
import com.bg.lovematchingapi.model.LoveLine.LoveLinePhoneNumberChangeRequest;
import com.bg.lovematchingapi.service.LoveLineService;
import com.bg.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")
public class LoveLineController {
    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")
    public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreateRequest request) {
        Member member = memberService.getData(memberId);
        loveLineService.setLoveLine(member, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<LoveLineItem> getLoveLines() {
        return loveLineService.getLoveLines();
    }

    @PutMapping("/phone-nunber/love-line-id/{loveLineID}")
    public String putPhoneNumber(@PathVariable long loveLineID, @RequestBody LoveLinePhoneNumberChangeRequest request) {
        loveLineService.petPhoneNumber(loveLineID, request);

        return "Ok";
    }
}
