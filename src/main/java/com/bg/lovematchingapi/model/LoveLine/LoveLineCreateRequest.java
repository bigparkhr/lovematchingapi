package com.bg.lovematchingapi.model.LoveLine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineCreateRequest {
    private String lovePhoneNumber;
}

